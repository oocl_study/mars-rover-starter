O:
Today, I mainly learned how to write unit tests and how to understand TDD. Unit Test consists of three parts: Given, When and Then. TDD refers to driving development through testing.
R:
harvest-full
I:
Previously, I rarely wrote unit testing for my code, which often resulted in some avoidable bugs. By learning how to write unit tests and coming up with possible test cases in advance, we can make our code more strong. But I have one question: If there are many and many conditions to judge, it may lead to many methods for unit testing, which may lead to a decrease in development efficiency? (thought from tonight homework)
D:
In the following development, i will write test cases in advance before writing the code.