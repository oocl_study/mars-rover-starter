package com.afs.tdd;

import com.afs.tdd.command_pattern.BackCommand;

public enum Command {

    MOVE("Move"),
    TURN_LEFT("TurnLeft"),
    TURN_RIGHT("TurnRight"),

    BackCommand("BackCommand");

    private String name;


    Command(String name) {
        this.name = name;
    }
}
