package com.afs.tdd;

public class Location {
    private int locationX;
    private int locationY;
    private Direction direction;

    public Location(int locationX, int locationY, Direction direction) {

        this.locationX = locationX;
        this.locationY = locationY;
        this.direction = direction;
    }

    public int getX() {
        return this.locationX;
    }

    public int getY() {
        return this.locationY;
    }

    public Direction getDirection() {
        return this.direction;
    }

    public void setY(int locationY) {
        this.locationY = locationY;
    }

    public void setX(int locationX) {
        this.locationX = locationX;
    }

    public void setDirection(Direction north) {
        this.direction = north;
    }
}
