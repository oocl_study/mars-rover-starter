package com.afs.tdd;

import java.awt.*;

public class MarsRover {
    private Location location;

    public MarsRover(Location location) {
        this.location = location;
    }

    public void action(Command command) {
        if(command.equals(Command.MOVE)){
            moveAction();
        } else if(command.equals(Command.TURN_RIGHT)){
            turnRightAction();
        } else if(command.equals(Command.TURN_LEFT)){
            turnLeftAction();
        } else if(command.equals(Command.BackCommand)){
            backAction();
        }
    }

    public Location getLocation() {
        return this.location;
    }

    public void moveAction() {
        if (this.location.getDirection().equals(Direction.NORTH)) {
            this.location.setY(this.location.getY() + 1);
        } else if (this.location.getDirection().equals(Direction.EAST)) {
            this.location.setX(this.getLocation().getX() + 1);
        } else if (this.location.getDirection().equals(Direction.WEST)) {
            this.location.setX(this.getLocation().getX() - 1);
        } else if (this.location.getDirection().equals(Direction.SOUTH)) {
            this.location.setY(this.getLocation().getY() - 1);
        }
    }

    public void turnLeftAction() {
        if (this.location.getDirection().equals(Direction.NORTH)) {
            this.location.setDirection(Direction.WEST);
        } else if (this.location.getDirection().equals(Direction.EAST)) {
            this.location.setDirection(Direction.NORTH);
        } else if (this.location.getDirection().equals(Direction.WEST)) {
            this.location.setDirection(Direction.SOUTH);
        } else if (this.location.getDirection().equals(Direction.SOUTH)) {
            this.location.setDirection(Direction.EAST);
        }
    }

    public void turnRightAction() {
        if (this.location.getDirection().equals(Direction.NORTH)) {
            this.location.setDirection(Direction.EAST);
        } else if (this.location.getDirection().equals(Direction.EAST)) {
            this.location.setDirection(Direction.SOUTH);
        } else if (this.location.getDirection().equals(Direction.WEST)) {
            this.location.setDirection(Direction.NORTH);
        } else if (this.location.getDirection().equals(Direction.SOUTH)) {
            this.location.setDirection(Direction.WEST);
        }

    }

    public void backAction() {
        if (this.location.getDirection().equals(Direction.NORTH)) {
            this.location.setY(this.location.getY() - 1);
        } else if (this.location.getDirection().equals(Direction.EAST)) {
            this.location.setX(this.getLocation().getX() - 1);
        } else if (this.location.getDirection().equals(Direction.WEST)) {
            this.location.setX(this.getLocation().getX() + 1);
        } else if (this.location.getDirection().equals(Direction.SOUTH)) {
            this.location.setY(this.getLocation().getY() + 1);
        }
    }
}
