package com.afs.tdd.command_pattern;

import com.afs.tdd.MarsRover;

public abstract class AbstractCommand {

    protected  MarsRover rover ;
    

    public AbstractCommand(MarsRover rover){
        this.rover = rover;
    }

    abstract public void execute();
}
