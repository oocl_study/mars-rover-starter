package com.afs.tdd.command_pattern;

import com.afs.tdd.MarsRover;

public class BackCommand extends AbstractCommand{

    public BackCommand(MarsRover rover) {
        super(rover);
    }

    @Override
    public void execute() {
        rover.backAction();
    }
}
