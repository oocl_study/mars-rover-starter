package com.afs.tdd.command_pattern;

import com.afs.tdd.Command;

public class CommandExecutor {

    private AbstractCommand command;



    public void setCommand(AbstractCommand command){
        this.command = command;
    }

    public void execute(){
        command.execute();
    }
}
