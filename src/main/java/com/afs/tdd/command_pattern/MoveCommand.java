package com.afs.tdd.command_pattern;

import com.afs.tdd.MarsRover;

public class MoveCommand extends AbstractCommand {


    public MoveCommand(MarsRover rover) {
        super(rover);
    }

    @Override
    public void execute() {
        rover.moveAction();
    }
}
