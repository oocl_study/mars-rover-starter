package com.afs.tdd.command_pattern;

import com.afs.tdd.MarsRover;

public class TurnLeftCommand extends AbstractCommand{


    public TurnLeftCommand(MarsRover rover) {
        super(rover);
    }

    @Override
    public void execute() {
        rover.turnLeftAction();
    }
}
