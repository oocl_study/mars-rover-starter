package com.afs.tdd.command_pattern;

import com.afs.tdd.MarsRover;

public class TurnRightCommand extends AbstractCommand{

    public TurnRightCommand(MarsRover rover) {
        super(rover);
    }

    @Override
    public void execute() {
        rover.turnRightAction();
    }
}
