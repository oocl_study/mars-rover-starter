package com.afs.tdd;

import com.afs.tdd.command_pattern.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class MarsRoverTest {

    @Test
    public void should_only_plus_locationY_1_when_command_move_and_direction_north(){
        Location location = new Location(0, 0, Direction.NORTH);
        MarsRover rover = new MarsRover(location);

        rover.action(Command.MOVE);

        Assertions.assertEquals(0, rover.getLocation().getX());
        Assertions.assertEquals(1, rover.getLocation().getY());
        Assertions.assertEquals(Direction.NORTH, rover.getLocation().getDirection());
    }

    @Test
    public void should_only_plus_locationX_1_when_command_move_and_direction_east(){
        Location location = new Location(0, 0, Direction.EAST);
        MarsRover rover = new MarsRover(location);

        rover.action(Command.MOVE);

        Assertions.assertEquals(1, rover.getLocation().getX());
        Assertions.assertEquals(0, rover.getLocation().getY());
        Assertions.assertEquals(Direction.EAST, rover.getLocation().getDirection());
    }

    @Test
    public void should_only_reduce_locationX_1_when_command_move_and_direction_west(){
        Location location = new Location(0, 0, Direction.WEST);
        MarsRover rover = new MarsRover(location);

        rover.action(Command.MOVE);

        Assertions.assertEquals(-1, rover.getLocation().getX());
        Assertions.assertEquals(0, rover.getLocation().getY());
        Assertions.assertEquals(Direction.WEST, rover.getLocation().getDirection());
    }

    @Test
    public void should_only_reduce_locationY_1_when_command_move_and_direction_South(){
        Location location = new Location(0, 0, Direction.SOUTH);
        MarsRover rover = new MarsRover(location);

        rover.action(Command.MOVE);

        Assertions.assertEquals(0, rover.getLocation().getX());
        Assertions.assertEquals(-1, rover.getLocation().getY());
        Assertions.assertEquals(Direction.SOUTH, rover.getLocation().getDirection());
    }

    @Test
    public void should_change_direction_to_north_when_command_turnRight_and_direction_west(){
        Location location = new Location(0, 0, Direction.WEST);
        MarsRover rover = new MarsRover(location);

        rover.action(Command.TURN_RIGHT);

        Assertions.assertEquals(0, rover.getLocation().getX());
        Assertions.assertEquals(0, rover.getLocation().getY());
        Assertions.assertEquals(Direction.NORTH, rover.getLocation().getDirection());
    }

    @Test
    public void should_change_direction_to_north_when_command_turnLeft_and_direction_east(){
        Location location = new Location(0, 0, Direction.EAST);
        MarsRover rover = new MarsRover(location);

        rover.action(Command.TURN_LEFT);

        Assertions.assertEquals(0, rover.getLocation().getX());
        Assertions.assertEquals(0, rover.getLocation().getY());
        Assertions.assertEquals(Direction.NORTH, rover.getLocation().getDirection());
    }

    @Test
    public void should_change_direction_to_east_when_command_turnRight_and_direction_north(){
        Location location = new Location(0, 0, Direction.NORTH);
        MarsRover rover = new MarsRover(location);

        rover.action(Command.TURN_RIGHT);

        Assertions.assertEquals(0, rover.getLocation().getX());
        Assertions.assertEquals(0, rover.getLocation().getY());
        Assertions.assertEquals(Direction.EAST, rover.getLocation().getDirection());
    }

    @Test
    public void should_change_direction_to_east_when_command_turnLeft_and_direction_south(){
        Location location = new Location(0, 0, Direction.SOUTH);
        MarsRover rover = new MarsRover(location);

        rover.action(Command.TURN_LEFT);

        Assertions.assertEquals(0, rover.getLocation().getX());
        Assertions.assertEquals(0, rover.getLocation().getY());
        Assertions.assertEquals(Direction.EAST, rover.getLocation().getDirection());
    }

    @Test
    public void should_change_direction_to_south_when_command_turnRight_and_direction_east(){
        Location location = new Location(0, 0, Direction.EAST);
        MarsRover rover = new MarsRover(location);

        rover.action(Command.TURN_RIGHT);

        Assertions.assertEquals(0, rover.getLocation().getX());
        Assertions.assertEquals(0, rover.getLocation().getY());
        Assertions.assertEquals(Direction.SOUTH, rover.getLocation().getDirection());
    }

    @Test
    public void should_change_direction_to_south_when_command_turnLeft_and_direction_west(){
        Location location = new Location(0, 0, Direction.WEST);
        MarsRover rover = new MarsRover(location);

        rover.action(Command.TURN_LEFT);

        Assertions.assertEquals(0, rover.getLocation().getX());
        Assertions.assertEquals(0, rover.getLocation().getY());
        Assertions.assertEquals(Direction.SOUTH, rover.getLocation().getDirection());
    }

    @Test
    public void should_change_direction_to_west_when_command_turnRight_and_direction_south(){
        Location location = new Location(0, 0, Direction.SOUTH);
        MarsRover rover = new MarsRover(location);

        rover.action(Command.TURN_RIGHT);

        Assertions.assertEquals(0, rover.getLocation().getX());
        Assertions.assertEquals(0, rover.getLocation().getY());
        Assertions.assertEquals(Direction.WEST, rover.getLocation().getDirection());
    }

    @Test
    public void should_change_direction_to_west_when_command_turnLeft_and_direction_north(){
        Location location = new Location(0, 0, Direction.NORTH);
        MarsRover rover = new MarsRover(location);

        rover.action(Command.TURN_LEFT);

        Assertions.assertEquals(0, rover.getLocation().getX());
        Assertions.assertEquals(0, rover.getLocation().getY());
        Assertions.assertEquals(Direction.WEST, rover.getLocation().getDirection());
    }

    @Test
    public void should_change_location_when_multiply_command(){
        Location location = new Location(0, 0, Direction.NORTH);
        MarsRover rover = new MarsRover(location);

        rover.action(Command.MOVE);
        rover.action(Command.TURN_LEFT);
        rover.action(Command.MOVE);
        rover.action(Command.TURN_RIGHT);

        Assertions.assertEquals(-1, rover.getLocation().getX());
        Assertions.assertEquals(1, rover.getLocation().getY());
        Assertions.assertEquals(Direction.NORTH, rover.getLocation().getDirection());
    }

    @Test
    public void should_change_location_when_multiply_command_use_commandPattern(){
        Location location = new Location(0, 0, Direction.NORTH);
        MarsRover rover = new MarsRover(location);

        MoveCommand moveCommand = new MoveCommand(rover);
        TurnLeftCommand turnLeftCommand = new TurnLeftCommand(rover);
        TurnRightCommand turnRightCommand = new TurnRightCommand(rover);
        CommandExecutor commandExecutor = new CommandExecutor();

        commandExecutor.setCommand(moveCommand);
        commandExecutor.execute();

        commandExecutor.setCommand(turnLeftCommand);
        commandExecutor.execute();

        commandExecutor.setCommand(moveCommand);
        commandExecutor.execute();

        commandExecutor.setCommand(turnRightCommand);
        commandExecutor.execute();

        Assertions.assertEquals(-1, rover.getLocation().getX());
        Assertions.assertEquals(1, rover.getLocation().getY());
        Assertions.assertEquals(Direction.NORTH, rover.getLocation().getDirection());
    }

    @Test
    public void should_only_reduce_locationX_1_when_command_back_and_direction_east(){
        Location location = new Location(0, 0, Direction.EAST);
        MarsRover rover = new MarsRover(location);

        BackCommand backCommand = new BackCommand(rover);
        CommandExecutor commandExecutor = new CommandExecutor();

        commandExecutor.setCommand(backCommand);
        commandExecutor.execute();

        Assertions.assertEquals(-1, rover.getLocation().getX());
        Assertions.assertEquals(0, rover.getLocation().getY());
        Assertions.assertEquals(Direction.EAST, rover.getLocation().getDirection());

    }
}

